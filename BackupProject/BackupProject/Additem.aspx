﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Additem.aspx.cs" Inherits="BackupProject.Admin.Additem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
      .style1
        {
            width: 201px;
        }
        .style2
        {
            width: 216px;
        } 
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent1" runat="server">
    <div align="center" style="width: 842px">
    <h2>
      Adding Product Page
    </h2>
    <br /><br />
    <table>
    <tr>
      <td class="style1">Product Name: </td>
      <td class="style2"><asp:TextBox ID="t1" runat="server" Height="28px" Width="195px"></asp:TextBox></td>
    </tr>
    
    <tr>
      <td class="style1">Product Description: </td>
      <td class="style2"><asp:TextBox ID="t2" runat="server" Height="50px" Width="195px" 
              TextMode="MultiLine"></asp:TextBox></td>
    </tr>
    
    <tr>
      <td class="style1">Product Price: </td>
      <td class="style2"><asp:TextBox ID="t3" runat="server" Height="28px" Width="195px"></asp:TextBox></td>
    </tr>

    <tr>
      <td class="style1">Product Quantity: </td>
      <td class="style2"><asp:TextBox ID="t4" runat="server" Height="28px" Width="195px"></asp:TextBox></td>
    </tr>

    <tr>
      <td class="style1">Product Image: </td>
      <td class="style2"><asp:FileUpload ID="f1" runat="server" Height="28px" Width="195px" /></td>    
    </tr>


    <tr>
       <td></td>
      
       <td></td>
    </tr>


    <tr>
      <td align="center"> 
          <asp:Button ID="b1" runat="server" Text="ADD" onclick="b1_Click" 
              /></td>
      <td class="style2"> <asp:Button ID="b2" runat="server" Text="CANCEL" /></td>
         
    </tr>

    <tr>
      <td colspan="2" align="center">
          <asp:Label ID="Label1" runat="server" forecolor="Blue"></asp:Label></td>   
    </tr>
    </table><br /><h2>Product Table</h2>
     <asp:GridView ID="GridView1" runat="server" 
             AllowPaging="True" 
            AutoGenerateColumns="False" Height="342px" Width="621px" 
            DataKeyNames="pid" >
           
            <Columns>
               
                <asp:BoundField DataField="pid" HeaderText="pid" 
                    SortExpression="pid" InsertVisible="False" ReadOnly="True" />
                <asp:BoundField DataField="pname" HeaderText="pname" 
                    SortExpression="pname" />
                <asp:BoundField DataField="pdesc" HeaderText="pdesc" 
                    SortExpression="pdesc" />
                <asp:BoundField DataField="price" HeaderText="price" 
                    SortExpression="price" />
                <asp:BoundField DataField="qty" HeaderText="qty" SortExpression="qty" />
                <asp:TemplateField HeaderText="pimage" SortExpression="pimage">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("pimage") %>' Height="100px" Width="100px" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("pimage") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:NUSERConnectionString %>" 
            SelectCommand="SELECT * FROM [Addproduct]"></asp:SqlDataSource>
    </div>
   
    
</asp:Content>
