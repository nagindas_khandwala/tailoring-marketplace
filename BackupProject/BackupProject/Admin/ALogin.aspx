﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ALogin.aspx.cs" Inherits="BackupProject.Admin.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <style type="text/css">
        .style1
        {
            height: 27px;
        }
        .style2
        {
            height: 40px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin:200px">
      <table align="center" class="style1" 
            style="border:1px ridge black; height: 178px;" width="450px">
        <tr>
          <td align="center" colspan="2" class="style2">
            <asp:Label ID="Label1" runat="server" Font-Names="Italic" style="Font-weight:700" forecolor="Chocolate" Text="Designing T-Shirt Admin panel"></asp:Label>
          </td>
        
        </tr>
      

      <tr>
          <td align="center" style="width:50%">User Name: </td>
           <td><asp:TextBox ID="txtuser" runat="server" Height="28px"></asp:TextBox></td>
      </tr>

       <tr>
          <td align="center" style="width:50%">Password: </td>
           <td style="width:50px"><asp:TextBox ID="txtpass" runat="server" textmode="Password" 
                   Height="28px"></asp:TextBox></td>
      </tr>

       <tr>
          <td align="center" style="width:50%">&nbsp;</td>
           <td style="width:50px">
               <asp:Button ID="btnlogin" runat="server" Text="Login" 
                   onclick="btnlogin_Click" Height="32px" Width="52px" /></td>
      </tr>
      
      <tr>
          <td align="center" colspan="2"><hr />
              <asp:Label ID="lblalert" runat="server" forecolor="Red"></asp:Label>
          </td>
      </tr>

      </table>
    
    </div>
    </form>
</body>
</html>
