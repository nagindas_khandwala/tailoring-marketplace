﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

namespace BackupProject.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            String username = WebConfigurationManager.AppSettings["AdminUserName"];
            String password = WebConfigurationManager.AppSettings["AdminPassword"];
            if (txtuser.Text == username && txtpass.Text == password)
            {
                Session["ShoppingCartAdmin"] = "ShoppingCartAdmin";
                Response.Redirect("~/Additem.aspx");
            }
            else
                lblalert.Text = "Incorrect Username/Password";
        }

    }
}