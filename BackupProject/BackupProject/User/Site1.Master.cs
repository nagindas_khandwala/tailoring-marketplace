﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BackupProject.User
{
    public partial class Site1 : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                MultiView1.ActiveViewIndex = 0;
            }
            else
            {
                MultiView1.ActiveViewIndex = 1;
            }
            txtShoppingCart.Text =Convert.ToString(Session["numberInCart"]); 
        }




        protected void linklogout_Click(object sender, EventArgs e)
        {
            Session["Username"] = "";
            MultiView1.ActiveViewIndex = 0;
        }

        protected void txtShoppingCart_DataBinding(object sender, EventArgs e)
        {
            if (Session["numberInCart"] != null)
            {
                txtShoppingCart.Text = Session["numberInCart"].ToString();
            }
        }

        protected void txtShoppingCart_PreRender(object sender, EventArgs e)
        {
            if (Session["numberInCart"] != null)
            {
                txtShoppingCart.Text = Session["numberInCart"].ToString();
            }
        }

       
    }
}