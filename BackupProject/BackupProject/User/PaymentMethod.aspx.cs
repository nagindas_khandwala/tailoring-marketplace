﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BackupProject.User
{
    public partial class PaymentMethod : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public void PaymentMethods()
        {
            if (rbtnCashOnDelivery.Checked)
            {
                btnContinue.Visible = true;
            }
            if (rbtnOnlinePayment.Checked)
            {
                pnlOnlinePayment.Visible = true;
                if (rbtnCredit.Checked)
                {
                    pnlCredit.Visible = true;
                }
                else
                {
                    pnlDebit.Visible = true;
                }
            }
        }

        protected void rbtnCashOnDelivery_CheckedChanged(object sender, EventArgs e)
        {
            pnlCredit.Visible = false;
            pnlDebit.Visible = false;
            btnContinue.Visible = true;
            pnlOnlinePayment.Visible = false;

        }

        protected void rbtnOnlinePayment_CheckedChanged(object sender, EventArgs e)
        {
            pnlCredit.Visible = false;
            pnlDebit.Visible = false;
            btnContinue.Visible = false;
            pnlOnlinePayment.Visible = true;
        }

        protected void rbtnCredit_CheckedChanged(object sender, EventArgs e)
        {
            pnlCredit.Visible = true;
            pnlDebit.Visible = false;
            btnContinue.Visible = false;
            pnlOnlinePayment.Visible = true;
        }

        protected void rbtnDebit_CheckedChanged(object sender, EventArgs e)
        {
            pnlCredit.Visible = false;
            pnlDebit.Visible = true;
            btnContinue.Visible = false;
            pnlOnlinePayment.Visible = true;
        }
    }
}