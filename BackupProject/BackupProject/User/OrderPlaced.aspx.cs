﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


namespace BackupProject.User
{
    public partial class OrderPlaced : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["Customer"];
            dt.Columns.Add("paymentMethod", typeof(string));
            //how to add data in the newly created data

            dt.Rows[0]["paymentMethod"] = Session["payment"];

            //Fetching data from datatable 

            lbheading.Text =Convert.ToString(dt.Rows[0]["uname"]) + " !!!Your Order has been Added Successfully";
            lbmobile.Text= Convert.ToString(dt.Rows[0]["mobile"]);
            lbaddress.Text = Convert.ToString(dt.Rows[0]["address"]);
            //lbpayment.Text = Convert.ToString(dt.Rows[0]["paymentMethod"]);
            lbproducts.Text = Convert.ToString(dt.Rows[0]["totalProducts"]);
            lbprice.Text = Convert.ToString(dt.Rows[0]["totalPrice"]);

        }
    }
}