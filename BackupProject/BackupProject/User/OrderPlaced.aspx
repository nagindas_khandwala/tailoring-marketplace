﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Site1.Master" AutoEventWireup="true" CodeBehind="OrderPlaced.aspx.cs" Inherits="BackupProject.User.OrderPlaced" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .style1
        {
            height: 100px;
            text-align:left;
           
        }
        .style2
        {
            height: 31px;
            text-align:left;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="lbheading" runat="server"  ></asp:Label><br /><br />
   <asp:Label ID="Label1" runat="server" Text="REVIEW YOUR ORDER" ></asp:Label><br />
   <div style="margin:10px 10px 10px 10px;height:203px; width:750px;">
      <table style="height:197px; width:750px;">
        <tr >
          <td class="style2"><b>Shipping Address:</b></td>
          <td class="style2"><b>Payment Method:</b></td>
          <td class="style2"><b>Delivery Date:</b></td>
        </tr>

        <tr>
          <td class="style1">
              <asp:Label ID="lbaddress" runat="server"  ></asp:Label>
          <br />
          <b>Phone:</b>    <asp:Label ID="lbmobile" runat="server"  ></asp:Label>    
          </td>
          <td class="style1">
              <asp:Label ID="lbpayment" runat="server"  ></asp:Label></td>
          <td class="style1"></td>
        </tr>
      </table>
   </div>
   <div style="margin:10px 10px 10px 10px; width: 400px;">
      <br />
      <asp:Label ID="Label2" runat="server" Text="ORDER SUMMARY" ></asp:Label><br /><br />
      
       Total Products:<asp:Label ID="lbproducts" runat="server"></asp:Label><br />
      
       Total Price:<asp:Label ID="lbprice" runat="server"></asp:Label><br />
   </div>
   <br /><br />
</asp:Content>
