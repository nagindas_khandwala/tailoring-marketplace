﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace BackupProject.User
{
    public partial class AddToCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetMyCart();
        }

        private void GetMyCart()
        {
            DataTable dtProducts;
            if (Session["MyCart"] != null)
            {
                dtProducts = (DataTable)Session["MyCart"];
            }
            else
            {
                dtProducts = new DataTable();
            }
            if (dtProducts.Rows.Count > 0)
            {
                lblTotalProducts.Text = dtProducts.Rows.Count.ToString();
                Session["numberInCart"] = dtProducts.Rows.Count.ToString();
                dlCartProducts.DataSource = dtProducts;
                dlCartProducts.DataBind();
                UpdateTotalBill();


                pnlMyCart.Visible = true;
                

            }

            else
            {
                pnlEmptyCart.Visible = true;
                pnlMyCart.Visible = false;
                

                dlCartProducts.DataSource = null;
                dlCartProducts.DataBind();
                lblTotalProducts.Text = "0";
                lblTotalPrice.Text = "0";
                Session["numberInCart"] = "0";
            }
        }

        

        private void UpdateTotalBill()
        {
            long TotalPrice = 0;
            long TotalProducts = 0;
            foreach (DataListItem item in dlCartProducts.Items)
            {
                Label PriceLabel = item.FindControl("lblPrice") as Label;
                TextBox ProductQuantity = item.FindControl("txtProductQuantity") as TextBox;
                long ProductPrice = Convert.ToInt64(PriceLabel.Text) * Convert.ToInt64(ProductQuantity.Text);
                TotalPrice = TotalPrice + ProductPrice;
                TotalProducts = TotalProducts + Convert.ToInt32(ProductQuantity.Text);
            }
            lblTotalPrice.Text = Convert.ToString(TotalPrice);
            lblTotalProducts.Text = Convert.ToString(TotalProducts);
        }

        protected void btnRemoveFromCart_Click(object sender, EventArgs e)
        {
             
            string ProductID = Convert.ToInt16((((Button)sender).CommandArgument)).ToString();
            if (Session["MyCart"] != null)
            {
                DataTable dt = (DataTable)Session["MyCart"];

                DataRow drr = dt.Select("pid=" + ProductID + " ").FirstOrDefault();

                if (drr != null)
                {
                    dt.Rows.Remove(drr);

                    Session["MyCart"] = dt;
                }

                GetMyCart();
            }
        
        }

        protected void txtProductQuantity_TextChanged(object sender, EventArgs e)
        {
            TextBox txtQuantity = (sender as TextBox);

            DataListItem currentItem = (sender as TextBox).NamingContainer as DataListItem;
            HiddenField ProductID = currentItem.FindControl("hfProductID") as HiddenField;
            Label lblAvailableStock = currentItem.FindControl("lblAvailableStock") as Label;

            if (txtQuantity.Text == string.Empty || txtQuantity.Text == "0" || txtQuantity.Text == "1")
            {
                txtQuantity.Text = "1";
            }
            else
            {
                if (Session["MyCart"] != null)
                {

                    if (Convert.ToInt32(txtQuantity.Text) <= Convert.ToInt32(lblAvailableStock.Text))
                    {

                        DataTable dt = (DataTable)Session["MyCart"];

                        DataRow[] rows = dt.Select("pid='" + ProductID.Value + "'");
                        int index = dt.Rows.IndexOf(rows[0]);
                        dt.Rows[index]["Uqty"] = txtQuantity.Text;
                        Session["MyCart"] = dt;
                    }
                    else
                    {
                        lblAvailableStockAlert.Text = "Alert : Product Buyout should not be greater than Available Stock!!";
                        txtQuantity.Text = "1";
                    }
                }
            }

            UpdateTotalBill();
        }

        protected void btnContinueShopping_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/User/Product.aspx");
        }

        public DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            String Conn = ConfigurationManager.ConnectionStrings["UserDB"].ConnectionString.ToString();
            SqlConnection con = new SqlConnection(Conn);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, con);
            da.Fill(dt);

            con.Close();
            return dt;
        }
        protected void btnOrderPlaced_Click(object sender, EventArgs e)
        {
            if (Session["MyCart"] != null)
            {
                string query = "select * from Register where email='" + Session["Username"] + "'";
                DataTable dtUser = GetData(query);

                DataTable dt = new DataTable();
                dt.Columns.Add("uid", typeof(int));
                dt.Columns.Add("uname", typeof(string));
                dt.Columns.Add("email", typeof(string));
                dt.Columns.Add("mobile", typeof(string));
                dt.Columns.Add("address", typeof(string));
                dt.Columns.Add("totalProducts", typeof(string));
                dt.Columns.Add("totalPrice", typeof(string));


                DataRow dr = dt.NewRow();

                dr["uid"] = (dtUser.Rows[0]["uid"]);
                dr["uname"] = Convert.ToString(dtUser.Rows[0]["uname"]);
                dr["email"] = Convert.ToString(dtUser.Rows[0]["email"]);
                dr["mobile"] = Convert.ToString(dtUser.Rows[0]["mobile"]);
                dr["address"] = Convert.ToString(dtUser.Rows[0]["address"]);
                dr["totalProducts"] = lblTotalProducts.Text;
                dr["totalPrice"] = lblTotalPrice.Text;
                dt.Rows.Add(dr);
                Session["Customer"] = dt;
            }

            Response.Redirect("~/User/PaymentMethod.aspx");
            
        }
        
        


        

    }

}