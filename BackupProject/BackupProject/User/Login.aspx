﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Site1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BackupProject.User.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
         Please enter Email Id and Password.<br />
       </p>
     
      <table align="center"  
            style=" height: 269px; width: 460px;">
        <tr>
          <td align="center" bgcolor="#6666ff" colspan="2" class="style1" >
            <asp:Label ID="Label1" runat="server" font-bold="true" forecolor="Black" Text="LOG IN"></asp:Label>
          </td>
           <td></td>       
        </tr>
      

      <tr>
          <td align="center" style="width:50%">Email Id: </td>
           <td align="center" style="width:50%"><asp:TextBox ID="txtemail" runat="server" Height="28px"></asp:TextBox></td>
          <td>
               <asp:RequiredFieldValidator ID="Rvalidateemail" runat="server" ErrorMessage="Email Id is Required"  ForeColor="Red" ControlToValidate="txtemail"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidatoremail" runat="server" 
                   ErrorMessage="Invalid Email Id" Display="Dynamic" ForeColor="Red" 
                   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail"></asp:RegularExpressionValidator>
          </td>
      </tr>

       <tr>
          <td align="center" style="width:50%">Password: </td>
           <td align="center" style="width:50px"><asp:TextBox ID="txtpass" runat="server" textmode="Password" 
                   Height="28px"></asp:TextBox></td>
                   <td>
           <asp:RequiredFieldValidator ID="Rvalidatepass" runat="server" ErrorMessage="Password is Required"  ForeColor="Red" ControlToValidate="txtpass"></asp:RequiredFieldValidator>
           </td>
      </tr>

       <tr>
          
           <td align="center" colspan="2">
               <asp:Button ID="btnlogin" runat="server" Text="Login" 
                   onclick="btnlogin_Click" Height="32px" Width="52px" /></td>
                   <td></td>
          
      </tr>
      
      <tr>
          <td align="center" colspan="2"><hr />
              <asp:Label ID="lblalert" runat="server" forecolor="Red"></asp:Label>
              <td></td>
          </td>
          
      </tr>

      </table>
      <br />
      <a href="Sign.aspx"><u>Register</u></a> if you don't have account.
    
  

</asp:Content>
