﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Site1.Master" AutoEventWireup="true" CodeBehind="Sign.aspx.cs" Inherits="BackupProject.User.Sign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            height: 50px;
        }
        .style3
        {
            width: 150px;
        }
    .style4
    {
        width: 50%;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
     <table align="center" 
            style=" height: 320px; width:594px; margin-right: 0px;">
       <tr>
           <td align="center" bgcolor="#6666ff" colspan="2" class="style1">
               <asp:Label ID="lb1" runat="server" Text="CREATE A NEW ACCOUNT" ForeColor="Black" Font-Bold="true"></asp:Label></td>
               <td class="style3">&nbsp;&nbsp;</td>
       </tr>
       
       
        
       <tr>
           <td align="center" style="width:50%">Name: </td>
           <td align="center" class="style4"><asp:TextBox ID="txtname" runat="server" 
                   Height="28px" Width="230px" ></asp:TextBox></td>
           <td class="style3">    <asp:RequiredFieldValidator ID="RequiredFieldValidatorname" runat="server" 
                   ControlToValidate="txtname" ErrorMessage="*" 
                   ForeColor="Red" Display="Dynamic" ToolTip="Name is required"></asp:RequiredFieldValidator></td>
           
      </tr>

      <tr>
           <td align="center" style="width:50%">Email: </td>
           <td align="center" class="style4"><asp:TextBox ID="txtemail" runat="server" Height="28px" Width="230px"></asp:TextBox></td>
            <td class="style3"><asp:RequiredFieldValidator ID="RequiredFieldValidatoremail" runat="server" 
                   ControlToValidate="txtemail" ErrorMessage="*" 
                   ForeColor="Red" Display="Dynamic" ToolTip="Email Id is required"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatoremail" 
                    runat="server" ErrorMessage="Invalid Email Id" ControlToValidate="txtemail" 
                    Display="Dynamic" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
           </td>
      </tr>

      <tr>
           <td align="center" style="width:50%">Mobile No.: </td>
           <td align="center" class="style4"><asp:TextBox ID="txtmob" runat="server" 
                   Height="28px" Width="230px" MaxLength="10"></asp:TextBox></td>
            <td class="style3"><asp:RequiredFieldValidator ID="RequiredFieldValidatormob" runat="server" 
                   ControlToValidate="txtmob" ErrorMessage="*" 
                   ForeColor="Red" Display="Dynamic" ToolTip="Mobile Number is required"></asp:RequiredFieldValidator>
           </td>
      </tr>

      <tr>
           <td align="center" style="width:50%">Address: </td>
           <td align="center" class="style4"><asp:TextBox ID="txtaddress" runat="server" Height="80px" Width="230px" TextMode="MultiLine"></asp:TextBox></td>
            <td class="style3"><asp:RequiredFieldValidator ID="RequiredFieldValidatoraddress" runat="server" 
                   ControlToValidate="txtaddress" ErrorMessage="*" 
                   ForeColor="Red" Display="Dynamic" ToolTip="Address is required"></asp:RequiredFieldValidator>
           </td>
      </tr>

       <tr>
          <td align="center" style="width:50%">Password: </td>
           <td align="center" class="style4"><asp:TextBox ID="txtpass" runat="server" textmode="Password" Height="28px" Width="230px"></asp:TextBox></td>
            <td class="style3"><asp:RequiredFieldValidator ID="RequiredFieldValidatorpass" runat="server" 
                   ControlToValidate="txtpass" ErrorMessage="*" 
                   ForeColor="Red" Display="Dynamic" ToolTip="Password is required"></asp:RequiredFieldValidator>
           </td>
      </tr>

      <tr>
          <td align="center" style="width:50%">Confirm Password: </td>
           <td align="center" class="style4"><asp:TextBox ID="txtcpass" runat="server" textmode="Password" Height="28px" Width="230px"></asp:TextBox></td>
           <td class="style3"> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                   ControlToValidate="txtcpass" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
               <asp:CompareValidator ID="CompareValidatorcpass" runat="server" 
                   ErrorMessage="Password and Confirm Password must match" ForeColor="Red" 
                   ControlToCompare="txtpass" ControlToValidate="txtcpass" Display="Dynamic" ></asp:CompareValidator>
           </td>
      </tr>

      <tr>
        <td colspan="3"></td>
      </tr>

       <tr>
          <td align="center" colspan="2">
              <asp:Button ID="btnsign" runat="server" Text="Create User" 
                  Height="32px" Width="90px" onclick="btnsign_Click" />&nbsp;&nbsp;
              <asp:Button ID="btncancel" runat="server" Text="Cancel" Height="32px" 
                  Width="90px" />
          </td>
          <td class="style3">&nbsp;&nbsp;</td>
        </tr>  
               
      <tr>
      <td align="center" colspan="2">
     
          <asp:Label ID="lblalert" runat="server" ForeColor="Blue"></asp:Label>
      </td>
      </tr>
       
    </table>
    </div>
</asp:Content>
