﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/User/Site1.Master" AutoEventWireup="true" CodeBehind="AddToCart.aspx.cs" Inherits="BackupProject.User.AddToCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
  <asp:Panel ID="pnlMyCart" runat="server"  Height="700px" ScrollBars="Auto"  BorderColor="Black" 
                BorderStyle="Inset" BorderWidth="1px">
      
         <center>
             <asp:Label ID="Label1" runat="server" Text="Products in your Shopping Cart" ForeColor="Brown" Font-Size="Larger" Font-Bold="true"></asp:Label><br />
             <asp:Label ID="lblAvailableStockAlert" runat="server"></asp:Label></center><br /><br />
                <asp:DataList ID="dlCartProducts" runat="server"
                            Font-Bold="False" Font-Italic="False"
                           Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                            Width="1180px" CellPadding="4" ForeColor="#333333" 
             Height="500px" >
                       <ItemTemplate>
                         <div align="center">
                            <table cellspacing="1" border="1px" style="border: 1px ridge #9900FF; text-align: center; width: 800px;">
                              
                                 <tr>
                                <td>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("pimage") %>' 
                                        width= "160px" height= "140px" />
                               
                                </td>
                                   
                                <td ><b>Name:&nbsp;</b><br />
                                  <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("pname")%>' style="font-weight: 700">
                                  </asp:Label> 
                                </td>
                              
                             
                                <td>
                                <b>In Stock:&nbsp;</b><br />
                                    <asp:Label ID="lblAvailableStock" runat="server" Text='<%# Eval("qty") %>' 
                                       ToolTip="Available Stock" ForeColor="Red" Font-Bold="true"></asp:Label>
                                 </td> 
                                 <td><b>Quantity:&nbsp;</b><br />
                                 <asp:TextBox ID="txtProductQuantity" runat="server" Width="35px" Height="26px"  
                                    OnTextChanged="txtProductQuantity_TextChanged"  AutoPostBack="true" 
                                         Text='<%# Eval("Uqty") %>'></asp:TextBox>
                                    <asp:HiddenField ID="hfProductID" runat="server" Value='<%# Eval("pid") %>' />
                                 </td>  
                                 <td><b>Price:&nbsp;</b><br />
                                <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("price") %>'></asp:Label> 
                                  &nbsp;&nbsp;
                                    
                                </td>
                              
                              
                                <td>
                                  <%--<hr />--%>
                                  <asp:Button ID="btnRemoveFromCart" runat="server" CommandArgument='<%# Eval("pid") %>' 
                                  Text="RemoveFromCart"  BorderColor="Black" BorderStyle="Inset" BorderWidth="1px" 
                                   OnClick="btnRemoveFromCart_Click" CausesValidation="false" BackColor="#FF9900" 
                                        Font-Bold="True" ForeColor="Black" Height="31px" Width="150px" />
                                </td>
                              </tr>
                            </table>
                         </div>
                       </ItemTemplate>
                       <AlternatingItemStyle BackColor="White" />
                       <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                       <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <ItemStyle Width="33%" BackColor="#FFFBD6" ForeColor="#333333" />
                       <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                   </asp:DataList>
            
          <h3>Total Product:<asp:Label ID="lblTotalProducts" runat="server" ForeColor="Red"></asp:Label></h3><br />
          <h3>Total Price:<asp:Label ID="lblTotalPrice" runat="server"  ForeColor="Red"></asp:Label></h3><br />
          <br />
      <asp:Button ID="btnContinueShopping" runat="server" Text="Continue Shopping"  
             Height="34px" Width="141px" style="border-radius:10px" BackColor="#339933" 
             ForeColor="Black" onclick="btnContinueShopping_Click"/>&nbsp;&nbsp;
      <asp:Button ID="btnOrderPlaced" runat="server" Text="Place Order"  Height="34px" 
             Width="141px" style="border-radius:10px;" BackColor="#339933" 
             ForeColor="Black" onclick="btnOrderPlaced_Click" />
          
                                
                  
          </asp:Panel>
    <asp:Panel ID="pnlEmptyCart" runat="server" Visible="false">
       <br />
       <br />
       <br />
        <asp:Label ID="Label2" runat="server" Text="YOUR SHOPPING CART IS EMPTY" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
        <br /><br />
    </asp:Panel>
    
</asp:Content>
