﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Site1.Master" AutoEventWireup="true" CodeBehind="DetailProduct.aspx.cs" Inherits="BackupProject.User.DetailProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
 .header
 {
     font-size:100;
     font-weight:bold;
    
 }
 .header1
 {
     color:ButtonText;
     font-weight:bold;
 }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:DataList ID="DataList1" runat="server" >
      <ItemTemplate>
<div style="width:450px; height:500px;float:left; border:1px inset black">
    
    <asp:Image ID="PImage" runat="server" ImageUrl='<%#Eval("pimage") %>' Height="100%" Width="100%"/>
          
     
</div>
<div style="width:550px;height:300px;float:right;">
   <br /><br /><br /> <br /><br />
  <table style="width: 281px; height: 181px">
    <tr>
      
      <td colspan="2" align="center" style="border-bottom:1px inset blue"><asp:Label runat="server"  ID="lbpname" Text='<%#Eval("pname") %>' Font-Bold="true" Font-Size="Larger" Font-Italic="true"></asp:Label> 
       </td>
    </tr>
    <tr>
     <td align="center" class="header">Description:</td>
     <td align="left" class="inside"><asp:Label ID="lbdesc" runat="server" Text='<%#Eval("pdesc") %>' ></asp:Label></td>
    </tr>
    <tr>
     <td align="center" class="header">Stock:</td>
     <td align="left" class="inside"><asp:Label ID="lbstock" runat="server" Text='<%#Eval("qty") %>' ></asp:Label></td>
    </tr>
    <tr class="header1">
     <td align="right" >Price:Rs </td>
     <td align="left"><asp:Label ID="lbprice" runat="server" Text='<%#Eval("price") %>' ></asp:Label></td>
    </tr>
    <tr>
      <td colspan="2" align="center">
          <asp:DropDownList ID="DropDownList1" runat="server">
             
              <asp:ListItem>Select Size</asp:ListItem>
              <asp:ListItem>Smaller</asp:ListItem>
              <asp:ListItem>Small</asp:ListItem>
              <asp:ListItem>Large</asp:ListItem>
              <asp:ListItem>XLarge</asp:ListItem>
              <asp:ListItem>XXLarge</asp:ListItem>
            
          </asp:DropDownList>
      </td>
    </tr>
    <tr>

      
      <td colspan="2" align="center">
          <asp:Button ID="btnadd" runat="server" Text="Add To Cart" backcolor="Orange" 
              width="150px" Height="33px" onclick="btnadd_Click"/>
       </td>
    </tr>
    <tr>
    <td colspan="2">
        <asp:Label ID="lbalert" runat="server" ></asp:Label></td>
    </tr>
  </table>

</div>
</ItemTemplate>
</asp:DataList>
</asp:Content>
