﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="BackupProject.User.Demo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:DataList ID="DataList1" runat="server">
     <ItemTemplate>
     <div style="width:500px; height:500px;float:left;" >
        <table>
          <tr style="border:1px inset black">
            <td><asp:Image ID="PImage" runat="server" ImageUrl='<%#Eval("pimage") %>' /></td>
          </tr>
        </table>  
    </div>
    <div  style="width:400px;height:300px;float:right;">
    <center>
      <table >
        <tr>
          <td colspan="2" align="center"><asp:Label runat="server"  ID="lbpname" Text='<%#Eval("pname") %>' Font-Bold="true" font-size="X-Large" Font-Italic="true"></asp:Label> </td>
        </tr>
        <tr>
          <td align="center" >Description</td>
          <td align="left"><asp:Label ID="lbdesc" runat="server" Text='<%#Eval("pdesc") %>' ></asp:Label></td>
        </tr>
        <tr>
     <td align="center">Price: </td>
     <td align="left"><asp:Label ID="lbprice" runat="server" Text='<%#Eval("price") %>' ></asp:Label></td>
    </tr>
    <tr>
      
      <td colspan="2">
          <asp:Button ID="btnadd" runat="server" Text="Add To Cart"  />
       </td>
    </tr>
      </table>
      </center>
    </div>
     </ItemTemplate>
    </asp:DataList>
    </form>
</body>
</html>
