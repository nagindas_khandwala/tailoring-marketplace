﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace BackupProject.User
{
    public partial class Zoomproduct : System.Web.UI.Page
    {
        
        int Q;
        string size = "";
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
           
            Session["addtocart"] = "1";
            SqlConnection con = new SqlConnection(@"Data Source=meenakshi\sqlexpress;Initial Catalog=NUSER;Integrated Security=True");
            Q = Convert.ToInt32(Request.QueryString["q"]);
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select * from Addproduct where pid=" + Q;
            con.Open();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataList1.DataSource = dt;
            DataList1.DataBind();

        }

        public DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            String Conn = ConfigurationManager.ConnectionStrings["UserDB"].ConnectionString.ToString();
            SqlConnection con = new SqlConnection(Conn);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, con);
            da.Fill(dt);

            con.Close();
            return dt;
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
             string pid = Convert.ToString(Q);
            string ProductQuantity = "1";



            if (Session["Username"] == null)
                Response.Redirect("~/User/Login.aspx");

            //DropDownList curr = new DropDownList();
            //curr.DataSource = DataList1.FindControl("DropDownList1") as DropDownList;
            //curr.DataBind();
            //curr.SelectedIndex = 2;
            //Label lblalert1 = new Label();
            //lblalert1.Text = "";
           // DataList1.Controls.Add(lblalert1);
         //  DropDownList curr= DataList1.FindControl("DropDownList1")as DropDownList;
           //   Label lblalert1 = DataList1.FindControl("lbalert")as Label;
          /*  DataListItem currentItem = (sender as TextBox).NamingContainer as DataListItem;
            
            DropDownList ddlsize = currentItem.FindControl("DropDownListSize") as DropDownList;
            Label lbsize = currentItem.FindControl("lbalert") as Label;
           * */

            //if (curr.SelectedIndex == -1)
            //{
            //    lblalert1.Text = "Select Size";
            //}
            //size = curr.SelectedItem.Value;

            if (Session["MyCart"] == null)
            {
                string query = "select * from Addproduct where pid=" + pid;
                DataTable dtproduct = GetData(query);

                DataTable dt = new DataTable();
                dt.Columns.Add("pid", typeof(string));
                dt.Columns.Add("pname", typeof(string));
                dt.Columns.Add("pdesc", typeof(string));
                dt.Columns.Add("price", typeof(string));
                dt.Columns.Add("qty", typeof(string));
                dt.Columns.Add("pimage", typeof(string));
                dt.Columns.Add("Uqty", typeof(string));


                DataRow dr = dt.NewRow();

                dr["pid"] = pid;
                dr["pname"] = Convert.ToString(dtproduct.Rows[0]["pname"]);
                dr["pdesc"] = Convert.ToString(dtproduct.Rows[0]["pdesc"]);
                dr["price"] = Convert.ToString(dtproduct.Rows[0]["price"]);
                dr["qty"] = Convert.ToString(dtproduct.Rows[0]["qty"]);
                dr["pimage"] = Convert.ToString(dtproduct.Rows[0]["pimage"]);
                dr["Uqty"] = ProductQuantity;
                dt.Rows.Add(dr);
                Session["MyCart"] = dt;

                Session["numberInCart"] = dt.Rows.Count.ToString();
          
                lbalert.Text = "Added to the cart " + dt.Rows.Count.ToString(); ;
              


            }
            else
            {
                DataTable dt = (DataTable)Session["MyCart"];
                string query = "select * from Addproduct where pid=" + pid;
                DataTable dtproduct = GetData(query);

                DataRow dr = dt.NewRow();
                dr["pid"] = pid;
                dr["pname"] = Convert.ToString(dtproduct.Rows[0]["pname"]);
                dr["pdesc"] = Convert.ToString(dtproduct.Rows[0]["pdesc"]);
                dr["price"] = Convert.ToString(dtproduct.Rows[0]["price"]);
                dr["qty"] = Convert.ToString(dtproduct.Rows[0]["qty"]);
                dr["pimage"] = Convert.ToString(dtproduct.Rows[0]["pimage"]);
                dr["Uqty"] = ProductQuantity;
                dt.Rows.Add(dr);
                Session["MyCart"] = dt;
                Session["numberInCart"] = dt.Rows.Count.ToString();
                lbalert.Text = "Added to the cart" + dt.Rows.Count.ToString();
             

            }
        

            

        }

    } 
}