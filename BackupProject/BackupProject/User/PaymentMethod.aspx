﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Site1.Master" AutoEventWireup="true" CodeBehind="PaymentMethod.aspx.cs" Inherits="BackupProject.User.PaymentMethod" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br /><br />
<asp:Panel ID="pnlPaymentMethod" runat="server" >
        <asp:Label ID="Label3" runat="server" Text="PAYMENT METHOD" ForeColor="#FF6600" 
            Font-Bold="True" Font-Italic="True" Font-Size="X-Large"></asp:Label>
       
            <br /><br />
            <div align="center" style="height:500px;width:700px;border:1px inset grey">
                <asp:RadioButton ID="rbtnCashOnDelivery" runat="server" 
                    GroupName="PaymentMethod" Text="Cash On Delivery" Font-Bold="True" 
                    Font-Size="Larger" ForeColor="#993300" AutoPostBack="True" 
                    oncheckedchanged="rbtnCashOnDelivery_CheckedChanged" /><br />

                <asp:Button ID="btnContinue" runat="server" Text="Continue" BackColor="#FF9900" 
                ForeColor="Black" Font-Bold="True" Height="31px" Width="97px" Font-Size="Large" visible="false"/><br />

                <asp:RadioButton ID="rbtnOnlinePayment" runat="server" 
                    GroupName="PaymentMethod" Text="Online Payment" Font-Bold="True" 
                    Font-Size="Larger" ForeColor="#993300" AutoPostBack="True" 
                    oncheckedchanged="rbtnOnlinePayment_CheckedChanged"/><br /><br />

                  <asp:Panel ID="pnlOnlinePayment" runat="server" Visible="false">

                      <asp:RadioButton ID="rbtnCredit" runat="server" GroupName="OnlinePayment" 
                          Text="Credit Card"  Font-Bold="True" Font-Size="Large" ForeColor="#CC9900" 
                          AutoPostBack="True" oncheckedchanged="rbtnCredit_CheckedChanged" /><br />

                      <asp:Panel ID="pnlCredit" runat="server" Visible="false">

                          <asp:Label ID="Label4" runat="server" Text="Name on Card:" width="150px" Height="30px"></asp:Label>&nbsp;
                          <asp:TextBox ID="txtname" runat="server" width="150px" Height="30px"></asp:TextBox><br />

                          <asp:Label ID="Label5" runat="server" Text="Card Number:" width="150px" Height="30px"></asp:Label>&nbsp;
                          <asp:TextBox ID="txtnumber" runat="server" width="150px" Height="30px"></asp:TextBox><br />

                          <asp:Label ID="Label6" runat="server" Text="Expiration Date:" width="150px" Height="30px"></asp:Label>&nbsp;
                          <asp:TextBox ID="txtdate" runat="server" width="150px" Height="30px"></asp:TextBox><br />
                          <asp:Button ID="btnAddC" runat="server" Text="Add" BackColor="#FF6600" 
                              Font-Bold="True" Height="29px" Width="54px" />



                      </asp:Panel><br />
                      <asp:RadioButton ID="rbtnDebit" runat="server" GroupName="OnlinePayment" 
                          Text="Debit Card" Font-Bold="True" Font-Size="Large" ForeColor="#CC9900" 
                          AutoPostBack="True" oncheckedchanged="rbtnDebit_CheckedChanged"/>
                        <asp:Panel ID="pnlDebit" runat="server" Visible="false">
                          <asp:Label ID="Label7" runat="server" Text="Name on Card:" width="150px" Height="30px"></asp:Label>&nbsp;
                          <asp:TextBox ID="txtname1" runat="server" width="150px" Height="30px"></asp:TextBox><br />

                          <asp:Label ID="Label8" runat="server" Text="Card Number:" width="150px" Height="30px"></asp:Label>&nbsp;
                          <asp:TextBox ID="txtnumber1" runat="server" width="150px" Height="30px"></asp:TextBox><br />

                          <asp:Label ID="Label9" runat="server" Text="Expiration Date:" width="150px" Height="30px" ></asp:Label>&nbsp;
                          <asp:TextBox ID="txtdate1" runat="server" width="150px" Height="30px"></asp:TextBox><br />

                          <asp:Label ID="Label10" runat="server" Text="CCV:" width="150px" Height="30px" ></asp:Label>&nbsp;
                          <asp:TextBox ID="txtCCV" runat="server" width="150px" Height="30px"></asp:TextBox><br />
                          <asp:Button ID="btnAddD" runat="server" Text="Add" BackColor="#FF6600" 
                                Font-Bold="True" Height="35px" Width="57px" />
                      </asp:Panel><br />
                  </asp:Panel>
                
            </div>
    </asp:Panel>
</asp:Content>
