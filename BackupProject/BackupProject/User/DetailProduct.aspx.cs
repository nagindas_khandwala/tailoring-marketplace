﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;


namespace BackupProject.User
{
    public partial class DetailProduct : System.Web.UI.Page
    {
        int Q;
        string size;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["addtocart"] = "1";
            SqlConnection con = new SqlConnection(@"Data Source=meenakshi\sqlexpress;Initial Catalog=NUSER;Integrated Security=True");
            Q = Convert.ToInt32(Request.QueryString["q"]);
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select * from Addproduct where pid=" + Q;
            con.Open();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            DataList1.DataSource = dt;
            DataList1.DataBind();
        }
        public DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            String Conn = ConfigurationManager.ConnectionStrings["UserDB"].ConnectionString.ToString();
            SqlConnection con = new SqlConnection(Conn);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, con);
            da.Fill(dt);

            con.Close();
            return dt;
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            string pid = Convert.ToString(Q);
            string ProductQuantity = "1";
            DataListItem currentItem = (sender as TextBox).NamingContainer as DataListItem;
            DropDownList ddlsize = currentItem.FindControl("DropDownListSize") as DropDownList;
            Label lbsize = currentItem.FindControl("lbalert") as Label;
            if (ddlsize.SelectedIndex == 0)
            {
                lbsize.Text = "Select Size";
            }
            size = ddlsize.SelectedItem.Text;

            if (Session["MyCart"] == null)
            {
                string query = "select * from Addproduct where pid=" + pid;
                DataTable dtproduct = GetData(query);

                DataTable dt = new DataTable();
                dt.Columns.Add("pid");
                dt.Columns.Add("pname");
                dt.Columns.Add("pdesc");
                dt.Columns.Add("price");
                dt.Columns.Add("qty");
                dt.Columns.Add("Uqty");


                DataRow dr = dt.NewRow();

                dr["pid"] = pid;
                dr["pname"] = Convert.ToString(dtproduct.Rows[0]["pname"]);
                dr["ppdesc"] = Convert.ToString(dtproduct.Rows[0]["pdesc"]);
                dr["price"] = Convert.ToString(dtproduct.Rows[0]["price"]);
                dr["qty"] = Convert.ToString(dtproduct.Rows[0]["qty"]);
                dr["Uqty"] = ProductQuantity;
                dt.Rows.Add(dr);
                Session["MyCart"] = dt;

                Session["numberInCart"] = dt.Rows.Count.ToString();
                Label lbalert = currentItem.FindControl("lbalert") as Label;
                lbalert.Text = "Added to the cart";


            }
            else
            {
                DataTable dt = (DataTable)Session["MyCart"];
                string query = "select * from Addproduct where pid=" + pid;
                DataTable dtproduct = GetData(query);

                DataRow dr = dt.NewRow();
                dr["pid"] = pid;
                dr["pname"] = Convert.ToString(dtproduct.Rows[0]["pname"]);
                dr["ppdesc"] = Convert.ToString(dtproduct.Rows[0]["pdesc"]);
                dr["price"] = Convert.ToString(dtproduct.Rows[0]["price"]);
                dr["qty"] = Convert.ToString(dtproduct.Rows[0]["qty"]);
                dr["Uqty"] = ProductQuantity;
                dt.Rows.Add(dr);
                Session["MyCart"] = dt;
                Label lbalert = currentItem.FindControl("lbalert") as Label;
                lbalert.Text = "Added to the cart";


            }
        }
    }
}