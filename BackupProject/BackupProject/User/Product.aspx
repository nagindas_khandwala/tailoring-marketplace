﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/Site1.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="BackupProject.User.Product" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
     .style4{width:185px;text-align:center;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:panel runat="server" ScrollBars="Auto" BorderStyle="Inset" BorderWidth="1px">
  <asp:DataList ID="DataList1" runat="server"  RepeatColumns="4" Width="1012px"  
        Font-Bold="false" Font-Italic="False" Font-Overline="false">
    <ItemTemplate>
        <table cellspacing="1" class="style4" style="border:1px ridge #9900FF " >
          <tr>
              <td style="border-bottom-style:ridge;border-width:1px;border-color:#000000">
                  <asp:Label ID="lbpname" runat="server" Text='<%#Eval("pname") %>'  Font-Bold="true"></asp:Label>
              </td>
          </tr>
          <tr>
              <td > <a href='Zoomproduct.aspx?q=<%# Eval("pid") %>'><asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("pimage") %>' Height="100%" Width="100%"/>
              </a></td>
          </tr>
          <tr>
             <td>Price: Rs.<asp:Label ID="lbprice" runat="server" Text='<%#Eval("price") %>' ></asp:Label></td>
          </tr>
          
        </table>
    </ItemTemplate>
    </asp:DataList>
  </asp:panel>
    
</asp:Content>
