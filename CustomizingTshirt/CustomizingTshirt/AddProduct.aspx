﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="CustomizingTshirt.AddProduct" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <div align="center" style="width: 842px">
    <h2>
      Adding Product Page
    </h2>
    <br /><br />
    <table>
    <tr>
      <td class="style1">Product Name: </td>
      <td class="style2"><asp:TextBox ID="t1" runat="server" Height="28px" Width="195px"></asp:TextBox></td>
    </tr>
    
    <tr>
      <td class="style1">Product type: </td>
      <td class="style2"><asp:TextBox ID="t2" runat="server" Height="28px" Width="195px"></asp:TextBox></td>
    </tr>

    <tr>
      <td class="style1">Product Description: </td>
      <td class="style2"><asp:TextBox ID="t3" runat="server" Height="50px" Width="195px" 
              TextMode="MultiLine"></asp:TextBox></td>
    </tr>
    
    <tr>
      <td class="style1">Product Price: </td>
      <td class="style2"><asp:TextBox ID="t4" runat="server" Height="28px" Width="195px"></asp:TextBox></td>
    </tr>

    <tr>
      <td class="style1">Product Quantity: </td>
      <td class="style2"><asp:TextBox ID="t5" runat="server" Height="28px" Width="195px"></asp:TextBox></td>
    </tr>

    <tr>
      <td class="style1">Product Image: </td>
      <td class="style2"><asp:FileUpload ID="f1" runat="server" Height="28px" Width="195px" /></td>    
    </tr>


    <tr>
       <td></td>
      
       <td></td>
    </tr>


    <tr>
      <td align="center"> 
          <asp:Button ID="b1" runat="server" Text="ADD" onclick="b1_Click" 
              /></td>
      <td class="style2"> <asp:Button ID="b2" runat="server" Text="CANCEL" /></td>
         
    </tr>

    <tr>
      <td colspan="2" align="center">
          <asp:Label ID="lbalert" runat="server" forecolor="Blue"></asp:Label></td>   
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
