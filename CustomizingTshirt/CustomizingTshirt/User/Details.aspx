﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/User/Site.Master" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="CustomizingTshirt.User.Details" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div style="width:450px; height:500px;float:left; border:1px inset black">
    
    <asp:Image ID="PImage" runat="server"  Height="100%" Width="100%" />
          
     
</div>


<div style="width:550px;height:300px;float:right;">
   <br /><br /><br /> <br /><br />
  <table style="width: 281px; height: 181px">
    <tr>
      
      <td colspan="2" align="center" style="border-bottom:1px inset blue"><asp:Label runat="server"  ID="lbpname"  Font-Bold="true" Font-Size="Larger" Font-Italic="true"></asp:Label> 
       </td>
    </tr>
    <tr>
     <td align="center" class="header">Description:</td>
     <td align="left" class="inside"><asp:Label ID="lbdesc" runat="server"  ></asp:Label></td>
    </tr>
    <tr>
     <td align="center" class="header">Stock:</td>
     <td align="left" class="inside"><asp:Label ID="lbstock" runat="server"  ></asp:Label></td>
    </tr>
    <tr class="header1">
     <td align="center" >Price: </td>
     <td align="left"><asp:Label ID="lbprice" runat="server"  ></asp:Label></td>
    </tr>
   <%-- <tr>
      <td colspan="2" align="center">
          <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
             
              
              <asp:ListItem Selected="True">Smaller</asp:ListItem>
              <asp:ListItem>Small</asp:ListItem>
              <asp:ListItem>Large</asp:ListItem>
              <asp:ListItem>XLarge</asp:ListItem>
              <asp:ListItem>XXLarge</asp:ListItem>
            
          </asp:DropDownList>
      </td>
    </tr>--%>

    <tr>
      <td colspan="2">
          <asp:DataList ID="DataList1" runat="server" RepeatDirection="Horizontal" >
              <ItemTemplate>
                  <asp:ImageButton ID="ImageButton1" runat="server" 
                      ImageUrl='<%#Eval("productimage") %>' height="50px" Width="50px" 
                      onclick="ImageButton1_Click" CommandArgument='<%#Eval("colorid") %>'/>
              </ItemTemplate>
          
          </asp:DataList>
      </td>
    </tr>

    <%--<tr>
    
      
      <td colspan="2" align="center">
          <asp:Button ID="btnadd" runat="server" Text="Add To Cart" backcolor="Orange" 
              width="150px" Height="33px" onclick="btnadd_Click"/>
       </td>
    </tr>--%>
   
       
  </table>
   

</div>
</asp:Content>
