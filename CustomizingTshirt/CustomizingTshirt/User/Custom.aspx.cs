﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CustomizingTshirt.User
{
    public partial class Custom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnkHalf_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/User/Products.aspx?q=1");
        }

        protected void lnlLong_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/User/Products.aspx?q=2");
        }
    }
}