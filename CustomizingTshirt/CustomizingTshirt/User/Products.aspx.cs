﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CustomizingTshirt.User
{
    public partial class Products : System.Web.UI.Page
    {
        int Q;
        string conn = ConfigurationManager.ConnectionStrings["NewDB"].ConnectionString;
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            string ptype;
             Q=Convert.ToInt32(Request.QueryString["q"]);
             if (Q == 1)
             {
                 ptype = "Short Sleeve";
                 showproduct(ptype);
             }
             else if (Q == 2)
             {
                 ptype = "Long Sleeve";
                 showproduct(ptype);
             }
        }
        public void showproduct(string ptype)
        {
            using (con = new SqlConnection(conn))
            {
                con.Open();
                
                SqlCommand cmd = new SqlCommand("SP_ShowProducts", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter()
                    { 
                        ParameterName="@ptype",
                        Value=ptype
                    });
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);
                DataList1.DataSource = dt;
                DataList1.DataBind();

                
            }
        }
    }
}