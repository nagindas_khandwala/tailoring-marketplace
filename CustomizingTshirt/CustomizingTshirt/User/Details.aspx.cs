﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CustomizingTshirt.User
{
    public partial class Details : System.Web.UI.Page
    {
        int pid;
        string conn = ConfigurationManager.ConnectionStrings["NewDB"].ConnectionString;
        SqlConnection con;
        string type;
        protected void Page_Load(object sender, EventArgs e)
        {
            pid = Convert.ToInt32(Request.QueryString["id"]);
            DataTable dt;
            using (con = new SqlConnection(conn))
            {

                con.Open();
                string query = "select * from Products where pid=" + pid;
                SqlCommand cmd = new SqlCommand(query, con);
                
                dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
               
                
            }
            type=Convert.ToString(dt.Rows[0]["ptype"]);
            lbpname.Text = Convert.ToString(dt.Rows[0]["pname"]);
            lbprice.Text = Convert.ToString(dt.Rows[0]["price"]);
            lbdesc.Text = Convert.ToString(dt.Rows[0]["pdesc"]);
            lbstock.Text = Convert.ToString(dt.Rows[0]["qty"]);
            PImage.ImageUrl = Convert.ToString(dt.Rows[0]["pimage"]);
            if (dt.Rows[0]["ptype"].Equals("Short Sleeve"))
            {

                showShortColors();
            }
            if (dt.Rows[0]["ptype"].Equals("Long Sleeve"))
            {

                showLongColors();
            }



        }
        public void showShortColors()
        {
            using (con = new SqlConnection(conn))
            {
                con.Open();
                string query = "select * from ShortColors ";
                SqlCommand cmd = new SqlCommand(query, con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                DataList1.DataSource = dt;
                DataList1.DataBind();
            }

        }

        public void showLongColors()
        {
            using (con = new SqlConnection(conn))
            {
                con.Open();
                string query = "select * from LongColors ";
                SqlCommand cmd = new SqlCommand(query, con);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                DataList1.DataSource = dt;
                DataList1.DataBind();
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt;
            string colorid = Convert.ToInt16((((ImageButton)sender).CommandArgument)).ToString();
            if (type.Equals("Short Sleeve"))
            {
                using (con = new SqlConnection(conn))
                {
                    con.Open();
                    string query = "select * from ShortColors where colorid=" + colorid;
                    SqlCommand cmd = new SqlCommand(query, con);
                    dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    PImage.ImageUrl = Convert.ToString(dt.Rows[0]["productimage"]);
                }
            }
            if (type.Equals("Long Sleeve"))
            {
                using (con = new SqlConnection(conn))
                {
                    con.Open();
                    string query = "select * from LongColors where colorid=" + colorid;
                    SqlCommand cmd = new SqlCommand(query, con);
                    dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    PImage.ImageUrl = Convert.ToString(dt.Rows[0]["productimage"]);
                }
            }
            
        }
    }
}