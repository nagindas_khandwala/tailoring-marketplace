﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CustomizingTshirt
{
    public partial class AddProduct : System.Web.UI.Page
    {
        string conn = ConfigurationManager.ConnectionStrings["NewDB"].ConnectionString;
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void b1_Click(object sender, EventArgs e)
        {
            if (f1.HasFile)
            {



                string str = f1.FileName;
                f1.PostedFile.SaveAs(Server.MapPath(".") + "//productimages1//" + str);
                string pimage = "~//productimages1//" + str.ToString();


                String pname = t1.Text;
                String ptype = t2.Text;
                String qty = t5.Text;
                String pdesc = t3.Text;
                Double price = Convert.ToDouble(t4.Text);

                using (con = new SqlConnection(conn))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("SP_Products", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@pname",
                        Value = pname
                    });
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "ptype",
                        Value = ptype
                    });
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@pdesc",
                        Value = pdesc
                    });

                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@price",
                        Value = price
                    });
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@qty",
                        Value = qty
                    });
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@pimage",
                        Value = pimage
                    });
                    cmd.ExecuteNonQuery();
                    lbalert.Text = "Product Added Successfully";

                }
            }
        }
    }
    }